﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gurobi;

namespace ShippmentAllocation.Models
{
    public class Allocation
    {
        private GRBEnv env;
        private GRBModel model;
        private Dictionary<Customer, GRBLinExpr> _pushed = null;
        private Dictionary<Store, GRBLinExpr> _pulled = null;

        public Allocation()
        {
            // Model
            env = new GRBEnv();
            model = new GRBModel(env);
            model.Set(GRB.StringAttr.ModelName, "Allocation");
        }

        public Dictionary<Customer, GRBLinExpr> Pushed(Customer[] customers, GRBLinExpr expression)
        {
            Dictionary<Customer, GRBLinExpr> dict = new Dictionary<Customer, GRBLinExpr>();
            foreach (var customer in customers)
                dict.Add(customer, expression);

            return dict;
        }

        public Dictionary<Store, GRBLinExpr> Pulled(Store[] stores, GRBLinExpr expression)
        {
            Dictionary<Store, GRBLinExpr> dict = new Dictionary<Store, GRBLinExpr>();
            foreach (var store in stores)
                dict.Add(store, expression);

            return dict;
        }

        public List<KeyValuePair<Store, Customer>> CreateLanes(Store[] stores, Customer[] customers)
        {
            Dictionary<Store,Customer> dict = new Dictionary<Store,Customer>();

            List<KeyValuePair<Store, Customer>> list = new List<KeyValuePair<Store, Customer>>();
            foreach(var store in stores)
            {
                foreach (var customer in customers)
                {
                    var elem = new KeyValuePair<Store, Customer>(store, customer);
                    list.Add(elem);
                }
            }

            return list;
        }

        public void CreateVariables(List<KeyValuePair<Store, Customer>> lanes, Dictionary<Store, GRBLinExpr> pulled, Dictionary<Customer, GRBLinExpr> pushed)
        {
            GRBVar [] container = new GRBVar[lanes.Count()];
            model.Set(GRB.IntAttr.ModelSense, 1);

            foreach (var lane in lanes)
            {
                GRBVar gvar = model.AddVar(0, GRB.INFINITY, Manager.Cost(lane.Value, lane.Key, 0.58), GRB.CONTINUOUS, lane.Key.Name+":"+lane.Value.Id.ToString());

                pulled[lane.Key] += gvar;
                pushed[lane.Value] += gvar;
            }

            _pushed = pushed;
            _pulled = pulled;
            model.Update();
        }

        public void AddDemandConstraints(Customer [] customers)
        {
            foreach(var customer in customers)
            {
                var temp = _pushed[customer];
                model.AddConstr(_pushed[customer] == customer.demand, "demand."+customer.Id.ToString());
            }
            model.Update();
        }

        public void AddSupplyConstraints(Store [] stores)
        {
            foreach(var store in stores)
            {
                model.AddConstr(_pulled[store] <= store.Supply, "supply." + store.Name);
            }
            model.Update();
        }

        public void WriteModel(string name)
        {
            model.Write(name);
        }

        public void Optimize()
        {
            model.Optimize();

            int status = model.Get(GRB.IntAttr.Status);
            if (status == GRB.Status.UNBOUNDED)
            {
                Console.WriteLine("The model cannot be solved "
                    + "because it is unbounded");
                return;
            }
            if (status == GRB.Status.OPTIMAL)
            {
                Console.WriteLine("The optimal objective is " +
                    model.Get(GRB.DoubleAttr.ObjVal));
                return;
            }
            if ((status != GRB.Status.INF_OR_UNBD) &&
                (status != GRB.Status.INFEASIBLE))
            {
                Console.WriteLine("Optimization was stopped with status " + status);
                return;
            }

            if(status == 3)
            {
                // Do IIS
                Console.WriteLine("The model is infeasible; computing IIS");
                model.ComputeIIS();
                Console.WriteLine("\nThe following constraint(s) "
                    + "cannot be satisfied:");
                foreach (GRBConstr c in model.GetConstrs())
                {
                    if (c.Get(GRB.IntAttr.IISConstr) == 1)
                    {
                        Console.WriteLine(c.Get(GRB.StringAttr.ConstrName));
                    }
                }
            }
        }
    }
}
