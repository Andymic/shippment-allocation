﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShippmentAllocation.Models
{
    public class Store
    {
        public string Name;
        public Coordinates Coordinate;
        public int Supply;
    }
}
