﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Device.Location;

namespace ShippmentAllocation.Models
{
    public static class Manager
    {
        public static Customer [] GetCustomers()
        {
            Customer cust1 = new Customer();
            cust1.Id = 1;
            cust1.demand = 40;
            cust1.Coordinate = new Coordinates(35.166, -101.886);

            Customer cust2 = new Customer();
            cust2.Id = 2;
            cust2.demand = 70;
            cust2.Coordinate = new Coordinates(29.298, -94.792);

            Customer cust3 = new Customer();
            cust3.Id = 3;
            cust3.demand = 58;
            cust3.Coordinate = new Coordinates(31.552, -97.139);

            Customer cust4 = new Customer();
            cust4.Id = 4;
            cust4.demand = 89;
            cust4.Coordinate = new Coordinates(31.857, -102.352);

            Customer cust5 = new Customer();
            cust5.Id = 5;
            cust5.demand = 37;
            cust5.Coordinate = new Coordinates(31.552, -97.139);

            return new Customer[] { cust1, cust2, cust3, cust4, cust5 };
        }

        public static Store [] GetStores()
        {
            Store store1 = new Store();
            store1.Name = "Dick's Boca Raton";
            store1.Supply = 75;
            store1.Coordinate = new Coordinates(30.271, -97.742);

            Store store2 = new Store();
            store2.Name = "Dick's West Palm Beach";
            store2.Supply = 50;
            store2.Coordinate = new Coordinates(31.758, -106.478);

            Store store3 = new Store();
            store3.Name = "Dick's Jupiter";
            store3.Supply = 135;
            store3.Coordinate = new Coordinates(29.616, -90.625);

            return new Store[]{store1,store2,store3};
        }

        public static double Cost(Customer customer, Store store, double price)
        {
            var cCoord = new GeoCoordinate(customer.Coordinate.lon, customer.Coordinate.lat);
            var sCoord = new GeoCoordinate(store.Coordinate.lon, store.Coordinate.lat);

            return Math.Round(((cCoord.GetDistanceTo(sCoord)*price)/10000),2);
        }

        public static void Print(Customer [] customer)
        {
            foreach(var cust in customer)
            {
                Console.WriteLine("[ Id: " + cust.Id);
                Console.WriteLine(",Coordinates: " + cust.Coordinate.lon.ToString() + "," + cust.Coordinate.lat.ToString());
                Console.WriteLine(",Demand: " + cust.demand + "]");
                Console.WriteLine("");
            }
            
        }

        public static void Print(Store [] store)
        {
            foreach(var str in store)
            {
                Console.WriteLine("[ Name: " + str.Name);
                Console.WriteLine(",Coordinates: " + str.Coordinate.lon.ToString() + "," + str.Coordinate.lat.ToString());
                Console.WriteLine(",Suppy: " + str.Supply + "]");
                Console.WriteLine("");
            }
        }
    }
}
