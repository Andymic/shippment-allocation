﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShippmentAllocation.Models
{
    public class Coordinates
    {
        public double lon;
        public double lat;
        public Coordinates(double a, double b)
        {
            lon = a;
            lat = b;
        }
    }
}
