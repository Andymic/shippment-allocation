﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShippmentAllocation.Models;

namespace ShippmentAllocation
{
    class Program
    {
        static void Main(string[] args)
        {
            Customer[] customers = Manager.GetCustomers();
            Store[] stores = Manager.GetStores();

            Allocation alloc = new Allocation();

            //orders pushed from customer to a store which takes a gurobi linear expression
            var pushed= alloc.Pushed(customers, new Gurobi.GRBLinExpr());

            //orders pulled from store to customer which takes a gurobi linear expression
            var pulled = alloc.Pulled(stores, new Gurobi.GRBLinExpr());

            //create a permutation of stores to customer
            var lanes = alloc.CreateLanes(stores, customers);

            //Create gurobi variables & update pull and push
            alloc.CreateVariables(lanes, pulled, pushed);

            //Create constraints for supply 
            alloc.AddSupplyConstraints(stores);

            //Create constraints for demand
            alloc.AddDemandConstraints(customers);

            //Optimize model
            alloc.Optimize();


            //alloc.WriteModel("trans.lp");
            //Manager.Print(customers);
            //Manager.Print(stores);
            //Console.WriteLine(Manager.Cost(customers[0], stores[0], 1));
            //foreach (var c in cust)
            //    Console.WriteLine(c.Coordinate.lon.ToString() + "--" + c.Coordinate.lat.ToString());

           

        }
    }
}
